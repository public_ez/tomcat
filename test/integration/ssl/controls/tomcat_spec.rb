title 'SSL Tomcat'

control 'ssl-tomcat-01' do
  describe port(8005) do
    it { should be_listening }
  end

  describe port(8443) do
    it { should be_listening }
  end

  describe port(8009) do
    it { should be_listening }
  end

  describe command('curl --fail -k -u admin:Temp1234 https://localhost:8443/manager/html') do
    its('stdout') { should match /Tomcat Web Application Manager/ }
  end
end
