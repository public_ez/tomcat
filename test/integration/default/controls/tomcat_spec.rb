title 'Default Tomcat'

control 'default-tomcat' do
  impact 1.0
  title 'Verify Tomcat is installed correctly'

  describe port(8005) do
    it { should be_listening }
  end

  describe port(8080) do
    it { should be_listening }
  end

  describe port(8009) do
    it { should be_listening }
  end

  describe command('curl --fail -u admin:Temp1234 http://localhost:8080/manager/html') do
    its('stdout') { should match /Tomcat Web Application Manager/ }
  end
end
