require 'spec_helper'

shared_examples 'tomcat::base' do
  describe user('tomcat') do
    it { should exist }
    it { should belong_to_group 'tomcat' }
  end

  describe group('tomcat') do
    it { should exist }
  end

  describe service('tomcat') do
    it { should be_enabled }
    it { should be_running }
  end

  describe process('java') do
    it { should be_running }
    its(:user) { should eq 'tomcat' }
  end

  describe file('/etc/tomcat/tomcat.conf') do
    it { should be_owned_by('tomcat') }
    it { should be_grouped_into 'tomcat' }
    it { should be_mode 664 }
  end

  describe file('/opt/tomcat/conf/server.xml') do
    it { should be_owned_by('tomcat') }
    it { should be_grouped_into 'tomcat' }
    it { should be_mode 664 }
  end

  describe file('/opt/tomcat/conf/tomcat-users.xml') do
    it { should be_owned_by('tomcat') }
    it { should be_grouped_into 'tomcat' }
    it { should be_mode 660 }
  end

  describe file('/opt/tomcat/webapps/docs') do
    it { should_not exist }
  end

  describe file('/opt/tomcat/webapps/examples') do
    it { should_not exist }
  end

  describe file('/opt/tomcat/webapps/ROOT') do
    it { should_not exist }
  end
end
