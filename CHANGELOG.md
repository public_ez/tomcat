# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.2] - 2018-04-19
### Changed
- Tomcat to v8.0.49 from v8.0.37
- Tomcat installed to version-specific directory
- Symlink pointing from version-agnostic directory to version-specific directory
- ServerSpec tests to InSpec tests

## [0.0.1]
### Added
- Initial role that installs and configures tomcat 7
- Test directory that allows to run a test playbook and serverspec from jenkins
- CHANGELOG file that follows the keepachangelog.com format
- README that describes how to use the role
