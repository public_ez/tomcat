Tomcat
======

Installs a redistributable packaged version of Tomcat from Apache. 

On top of installing Tomcat it also installs [Apache Portable Runtime](https://apr.apache.org/) (APR) project and the [Tomcat Native library](https://tomcat.apache.org/native-doc/). These packages can allow you to use the SSL certs and pems instead of JAVA keystores.

If firewalld is present it will add basic firewall rules for the all the Tomcat ports.

Requirements
------------

Requires oracle-java to be installed

Role Variables
--------------

```
# Install Tomcat from package manager or archive (tar)
tomcat_install_from_archive: yes

# Tomcat version number
# NOTE: Version 7.0.54 is the latest version of Tomcat supported by RHEL7
tomcat_version: "8.0.37"

# Apache Portable Runtime versions (https://apr.apache.org/)
tomcat_apr_version: "1.4.8"
tomcat_apr_util_version: "1.5.2"

# Tomcat Native Library version (https://tomcat.apache.org/native-doc/)
tomcat_native_version: "1.1.34"

# Tomcat archive artifact
tomcat_artifact_url: "http://jenkins.example.local/artifactory/generic-3rd-party/apache/tomcat/{{ tomcat_version }}/apache-tomcat-{{ tomcat_version }}.tar.gz"
tomcat_artifact_sha256sum: 2616c94e7da31a773129d7eaba9da365dd6a298b5e0e5aa457b9f12fbb272d39

# Tomcat.conf
tomcat_security_manager: "false"
tomcat_lang: en_US
tomcat_shutdown_wait: 30
# Example JAVA options
#   tomcat_java_opts:
#     - "-Xminf0.1"
#     - "-Xmaxf0.3"
tomcat_java_opts: []
# Example CATALINA options
#   tomcat_java_opts:
#     - "-Xminf0.1"
#     - "-Xmaxf0.3"
tomcat_catalina_opts: []
# Example Tomcat settings
#   tomcat_settings:
#     SOME_KEY1: some_value1
#     SOME_KEY2: some_value2
tomcat_settings: {}

# Server xml options
tomcat_scheme: http
tomcat_server_port: "8005"
tomcat_connector_http_port: "8080"
tomcat_connector_https_port: "8443"
tomcat_connector_ajp_port: "8009"
# Server xml SSL options
# NOTE: If your going to be installing your own SSL keys
#       set tomcat_ssl_keys_generate to 'no'
tomcat_ssl_keys_generate: no  # Generates self signed keys for testing SSL
tomcat_ssl_certificate_file: "{{ tomcat_install_dir }}/temp/test_server.crt"      # Path to cert
tomcat_ssl_certificate_key_file: "{{ tomcat_install_dir }}/temp/test_server.pem"  # Path to private key
tomcat_ssl_password: ""  # Password for private key (optional)
tomcat_ssl_max_threads: 150
tomcat_ssl_verify_client: optional
tomcat_ssl_protocol: "TLSv1+TLSv1.1+TLSv1.2"

# Java and Tomcat Paths
tomcat_java_home: /usr/java/latest
tomcat_install_dir: /opt/tomcat
tomcat_config_dir: /etc/tomcat

# Tomcat system user
tomcat_user_name: tomcat
tomcat_group_name: tomcat
tomcat_user_uid: 91
tomcat_user_home: "{{ tomcat_install_dir }}"

# Tomcat web user
tomcat_admin_user: admin
tomcat_admin_password: Temp1234
```

Dependencies
------------

* oracle-java: http://jenkins.example.local/scm/efr/oracle-java.git
    - This role installs CA certificates in the java keystore
    - This role installs environment variables for all users of JAVA_HOME, JRE_HOME and amends JAVA binary location to the PATH

Example Playbook
----------------

Example base playbook

```
- hosts: tomcat
  roles:
    - role: oracle-java
    - role: tomcat
```

Example playbook with SSL

```
- hosts: tomcat
  roles:
    - role: oracle-java
    - role: tomcat
      tomcat_scheme: https
      tomcat_ssl_certificate_file: /etc/certs/SSLcert.crt
      tomcat_ssl_certificate_key_file: /etc/certs/SSLpriv.pem
      tomcat_ssl_password: SomePassWord1234
```
